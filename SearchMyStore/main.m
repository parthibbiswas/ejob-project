//
//  main.m
//  SearchMyStore
//
//  Created by APPLE on 15/03/17.
//  Copyright (c) 2017 ejobs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}

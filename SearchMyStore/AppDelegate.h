//
//  AppDelegate.h
//  SearchMyStore
//
//  Created by APPLE on 15/03/17.
//  Copyright (c) 2017 ejobs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

